package it.ancientrealms.beautyquestsbook;

import java.io.File;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;

import com.electronwill.nightconfig.core.file.FileConfig;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.IntegerArgument;
import fr.skytasul.quests.api.QuestsAPI;
import it.ancientrealms.beautyquestsbook.listener.Listener;
import it.ancientrealms.beautyquestsbook.manager.Manager;
import fr.skytasul.quests.structure.Quest;

public final class BeautyQuestsBook extends JavaPlugin
{
    private static BeautyQuestsBook INSTANCE;
    private Manager Manager;
    private FileConfig messages;

    @Override
    public void onEnable()
    {
        INSTANCE = this;

        this.Manager = new Manager();

        this.getServer().getPluginManager().registerEvents(new Listener(), this);

        this.saveResource("messages.yml", false);
        this.messages = FileConfig.builder(new File(this.getDataFolder(), "messages.yml"))
                .autoreload()
                .autosave()
                .build();
        this.messages.load();

        new CommandAPICommand("createquestbook")
                .withPermission("beautyquestsbook.command")
                .withArguments(new IntegerArgument("questId").replaceSuggestions(info -> {
                    return QuestsAPI.getQuests().stream().map(q -> String.valueOf(q.getID())).toArray(String[]::new);
                }))
                .executesPlayer((player, args) -> {
                    final int questid = (int) args[0];
                    final Quest quest = QuestsAPI.getQuestFromID(questid);

                    if (quest == null)
                    {
                        CommandAPI.fail(((String) this.messages.get("errors.quest_not_found")).formatted(questid));
                    }

                    final ItemStack itemstack = player.getInventory().getItemInMainHand();
                    final Material material = itemstack.getType();

                    if (!material.equals(Material.WRITTEN_BOOK))
                    {
                        CommandAPI.fail(this.messages.get("errors.invalid_item"));
                    }

                    final ItemMeta itemmeta = itemstack.getItemMeta();
                    final NamespacedKey key = new NamespacedKey(this, "questid");

                    itemmeta.getPersistentDataContainer().set(key, PersistentDataType.INTEGER, questid);
                    itemstack.setItemMeta(itemmeta);

                    player.sendMessage(((String) this.messages.get("info.book_created")).formatted(questid));
                })
                .register();
    }

    @Override
    public void onDisable()
    {
    }

    public static BeautyQuestsBook getInstance()
    {
        return INSTANCE;
    }

    public Manager getManager()
    {
        return this.Manager;
    }
}
