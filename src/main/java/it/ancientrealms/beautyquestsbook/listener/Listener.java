package it.ancientrealms.beautyquestsbook.listener;

import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import fr.skytasul.quests.api.QuestsAPI;
import fr.skytasul.quests.players.PlayerAccount;
import fr.skytasul.quests.players.PlayersManager;
import fr.skytasul.quests.structure.Quest;
import it.ancientrealms.beautyquestsbook.BeautyQuestsBook;

public final class Listener implements org.bukkit.event.Listener
{
    private final BeautyQuestsBook plugin = BeautyQuestsBook.getInstance();

    @EventHandler
    public void playerInteract(PlayerInteractEvent event)
    {
        if (!event.getMaterial().equals(Material.WRITTEN_BOOK))
        {
            return;
        }

        final Action action = event.getAction();

        if (!(action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)))
        {
            return;
        }

        final ItemStack itemstack = event.getItem();
        final ItemMeta itemmeta = itemstack.getItemMeta();
        final NamespacedKey key = new NamespacedKey(this.plugin, "questid");
        final Integer questid = itemmeta.getPersistentDataContainer().get(key, PersistentDataType.INTEGER);

        if (questid == null)
        {
            return;
        }

        final Player player = event.getPlayer();
        final UUID uuid = player.getUniqueId();

        if (this.plugin.getManager().hasUUID(uuid))
        {
            return;
        }

        final Quest quest = QuestsAPI.getQuestFromID(questid);
        final PlayerAccount playeraccount = PlayersManager.getPlayerAccount(player);

        if (!quest.isLauncheable(player, playeraccount, true))
        {
            return;
        }

        this.plugin.getManager().addUUID(uuid);

        plugin.getServer().getScheduler().runTaskLater(this.plugin, () -> {
            quest.start(player);
            this.plugin.getManager().removeUUID(uuid);
        }, 20 * 5);
    }
}
