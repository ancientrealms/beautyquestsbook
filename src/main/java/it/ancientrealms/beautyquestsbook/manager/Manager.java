package it.ancientrealms.beautyquestsbook.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class Manager
{
    private List<UUID> players = new ArrayList<>();

    public void addUUID(UUID uuid)
    {
        this.players.add(uuid);
    }

    public boolean hasUUID(UUID uuid)
    {
        return this.players.contains(uuid);
    }

    public void removeUUID(UUID uuid)
    {
        this.players.removeIf(p -> p.equals(uuid));
    }
}
